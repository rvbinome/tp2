#include "cercle.h"
#include "rectangle.h"
#include "carre.h"
#include <vector>

using namespace std;

class ListeForme {
	private :
		vector<Forme*> v; // vecteur qui contient les pointeurs formes

	public :
		ListeForme(); // constructeur

		vector<Forme*> getV() const { return v; } // getteur du vecteur

  		void enleverElement( unsigned int i); // supprime l'élément i du vecteur
		void ajoutElement( Forme * f); // ajoute une forme à la fin du vecteur
		void afficherListe(); // affiche le vecteur

		double surfaceTotale();

};
