#include "forme.h"

Forme::Forme(Point const& p) {
	origine.setX(p.getX());
	origine.setY(p.getY());
}

void Forme::operator += (Point const &p) {
	origine += p;
}

ostream& operator << (ostream& s, Forme const & f ) {
	f.afficheForme(s);
	return s;
}
