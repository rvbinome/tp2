#include "listeforme.h"

ListeForme::ListeForme() {
 // pas besoin d'initialisation
}

void ListeForme::ajoutElement( Forme *f) {
	v.push_back(f); // on ajoute l'élément f à la fin
}

void ListeForme::enleverElement( unsigned int i) {
	if ( i < v.size() )
		v.erase( v.begin() + i );
}

void ListeForme::afficherListe() {
	for ( unsigned int i; i<v.size(); i++ )
			cout<<&(v[i])<<endl;
}
