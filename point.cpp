#include "point.h"
using namespace std;

Point::Point() : x(0), y(0){

}

Point::Point( double _x, double _y) : x(_x) , y(_y ) {

}

Point::Point(Point const& p)  {
	x = p.getX();
	y = p.getY();
}

void Point::translater(double _x, double _y) {
	x += _x;
	y += _y;
}

void Point::translater(Point const& p) {
	x += p.x;
	y += p.y;
}

ostream& operator <<(ostream& s ,Point const& p) {
	s << "(" << p.getX() << "," << p.getY() << ")";
	return s;
}

void Point::operator +=(Point const& p) {
	x += p.x ;
	y += p.y ;
}
