#include "cercle.h"

Cercle::Cercle(Point const& p, double r) : Forme(p), rayon(r){

}

double Cercle::perimetre() {
	return 2*M_PI*rayon;
}
double Cercle::surface() {
	return M_PI*rayon*rayon;
}

void Cercle::afficheForme(ostream & s) {
	s << this;
}

ostream& operator <<(ostream& s ,Cercle const& c) {
	s << "Cercle : " << "(" << c.getOrigin() << "," << c.getR() << ")";
	return s;
}
