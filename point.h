#include <iostream>
using namespace std;

class Point {
	private :
		double x,y;

	public :
		Point();
		Point(double _x, double _y);
		Point(Point const& p);

		double getX() const{ return x; }
		double getY() const { return y; }
		void setX(int _x) { x = _x; }
		void setY(int _y) { y = _y; }

		void translater(double _x, double _y);
		void translater(Point const& p);

		void operator +=(Point const& p);


};

ostream& operator <<(ostream& s ,Point const& p);
