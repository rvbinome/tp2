#include "forme.h"

class Rectangle : public Forme {
	private:
		double longueur;
		double largeur;
	public:
		Rectangle(Point const &p, double lo, double la);
		double perimetre();
		double surface();
		void operator += (Point const &p);

		double getLo() const { return longueur; }
		double getLa() const { return largeur; }

		void afficheForme(ostream & s) const;

};


ostream& operator <<(ostream& s ,Rectangle const& r);

