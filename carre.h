#include "forme.h"

class Carre : public Forme {
	private:
		double cote;
	public:
		Carre(Point const &p, double c);
		double perimetre();
		double surface();
		void operator += (Point const &p);

		double getC() const { return cote; }

		void afficheForme(ostream & s) const;

};


ostream& operator <<(ostream& s ,Carre const& c);
