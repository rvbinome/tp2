#pragma once
#include "point.h"

class Forme {
	protected :
		Point origine;
	public :
		Forme(Point const &p);
		virtual double perimetre() = 0;
		virtual double surface() = 0;
		Point getOrigin() const { return origine; };
		virtual void afficheForme( ostream& s) const =0;

		void operator += (Point const & p);

};

ostream& operator << (ostream& s, Forme const & f );
