#define _USE_MATH_DEFINES
#include <cmath>
#include "forme.h"

class Cercle : public Forme {
	private:
		double rayon;
	public:
		Cercle(Point const &p, double r);
		double perimetre();
		double surface();
		void operator += (Point const &p);
		double getR() const { return rayon; }
		void afficheForme(ostream & s);
};


ostream& operator <<(ostream& s ,Cercle const& c);
