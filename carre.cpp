#include "carre.h"

Carre::Carre(Point const& p, double c) : Forme(p) cote(c) {

}

double Carre::perimetre() {
	return 4*cote;
}
double Carre::surface() {
	return cote*cote;
}

void Carre::afficheForme(ostream & s) const {
	s << this;
}

ostream& operator <<(ostream& s ,Carre const& c) {
	s << "Carre : " << "(" << c.getOrigin() << "," << c.getC() << ")";
	return s;
}
