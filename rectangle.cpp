#include "rectangle.h"

Rectangle::Rectangle(Point const& p, double lo, double la) : Forme(p) longueur(lo) largeur(la){

}

double Rectangle::perimetre() {
	return 2*(longueur+largeur);
}
double Rectangle::surface() {
	return longueur*largeur;
}
void Rectangle::afficheForme(ostream & s) const {
	s << this;
}

ostream& operator <<(ostream& s ,Rectangle const& r) {
	s << "Rectangle : " << "(" << r.getOrigin() << "," << r.getLo() << "," << r.getLa() << ")";
	return s;
}
